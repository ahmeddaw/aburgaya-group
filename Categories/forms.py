from django import forms

from Categories.models import Category


class AddCategoryForm(forms.ModelForm):
    name = forms.CharField(max_length=2048)

    name.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Name',
    }

    class Meta:
        model = Category
        fields = '__all__'
