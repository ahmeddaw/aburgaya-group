from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView
from Categories.forms import AddCategoryForm
from Categories.models import Category


class AddCategoryView(CreateView):
    template_name = 'admin/categories_add_form.html'
    model = Category
    form_class = AddCategoryForm

    def get_success_url(self):
        return reverse("Manager:manager_categories")


class EditCategoryView(UpdateView):
    template_name = 'admin/categories_add_form.html'
    model = Category
    form_class = AddCategoryForm

    def get_success_url(self):
        return reverse("Manager:manager_categories")


class CategoryDeleteView(DeleteView):
    model = Category
    success_url = reverse_lazy('Manager:manager_categories')
    template_name = 'admin/delete_form.html'
    extra_context = {
        'back_url': reverse_lazy('Manager:manager_categories')
    }
