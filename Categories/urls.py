from django.urls import path

from Categories.views import *

app_name = "Categories"

urlpatterns = [
    path('add', AddCategoryView.as_view(), name='add'),
    path('<int:pk>/edit', EditCategoryView.as_view(), name='edit'),
    path('<int:pk>/delete', CategoryDeleteView.as_view(), name='delete'),
]
