from django.contrib.auth.decorators import login_required
from django.urls import path
from .views import *

app_name = 'Company'
urlpatterns = [
    path('', AuthorsListView.as_view(), name='home'),
    path('<int:pk>/detail', AuthorsDetailView.as_view(), name='detail'),
    path('add', login_required(AddAuthorsView.as_view()), name='add'),
    path('<int:pk>/edit', login_required(EditAuthorsView.as_view()), name='edit'),
    path('<int:pk>/delete', login_required(DeleteAuthorsView.as_view()), name='delete'),
]
