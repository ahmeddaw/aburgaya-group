from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView, DetailView, ListView

from Company.forms import AddAuthorForm
from Company.models import Company
from Products.models import Product


class AuthorsListView(ListView):
    template_name = "authors_list.html"
    model = Company
    paginate_by = 10
    context_object_name = "authors"


class AuthorsDetailView(DetailView):
    template_name = "company_detail.html"
    model = Company
    context_object_name = 'company'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        obj = self.get_object(self.get_queryset())
        data['products'] = Product.objects.filter(company=obj)
        return data


class AddAuthorsView(CreateView):
    template_name = "admin/authors_add_form.html"
    model = Company
    form_class = AddAuthorForm

    def get_success_url(self):
        return reverse("Manager:manager_company")


class EditAuthorsView(UpdateView):
    template_name = "admin/authors_add_form.html"
    model = Company
    form_class = AddAuthorForm

    def get_success_url(self):
        return reverse("Manager:manager_company")


class DeleteAuthorsView(DeleteView):
    model = Company
    success_url = reverse_lazy('Manager:manager_authors')
    template_name = 'admin/delete_form.html'
    extra_context = {
        'back_url': reverse_lazy('Manager:manager_authors')
    }
