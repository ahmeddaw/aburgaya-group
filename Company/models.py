from django.db import models
from django.urls import reverse


class Company(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=2048)
    description = models.TextField(blank=True)
    image = models.FileField(default="man.svg", upload_to="images/authors")

    @property
    def url(self):
        return reverse("media", kwargs={'path': self.image.url})

    def __str__(self):
        return self.name
