from django import forms

from Company.models import Company


class AddAuthorForm(forms.ModelForm):
    name = forms.CharField(max_length=2048)
    description = forms.CharField(widget=forms.Textarea())
    image = forms.FileField()

    name.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Name',
    }

    description.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Description',
    }

    image.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Image',
    }

    class Meta:
        model = Company
        fields = '__all__'
