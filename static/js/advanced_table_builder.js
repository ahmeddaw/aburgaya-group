$(function () {


    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

});


let places = new Map();
let lectures = new Map();
let edited_lectures = [];
let host = "https://umelib.ly";
// let host = "http://0.0.0.0:8090";

const days = [
    "السبت",
    "اﻷحد",
    "اﻹثنين",
    "الثلاثاء",
    "اﻹربعاء",
    "الخميس"
];


function drawTable(table, section, postFunction) {

    let sectionLectures = lectures.get(section);
    let lecturesPerDay = filterByDay(sectionLectures, section);
    let tableHtml = "";


    // for (let dayLectures in lecturesPerDay) {

    let sectionPlaces = places.get(section).places;

    let rowSpan = sectionPlaces.length + 1;
    for (let i = 0; i < 6; i++) {
        let dayLectures = lecturesPerDay[i];

        tableHtml += "<tr day-scope='" + i + "'>";
        tableHtml += "<th rowspan='" + rowSpan + "' class='align-middle' scope='row'>";
        tableHtml += days[i];
        tableHtml += "</th>";
        tableHtml += prepareDay(dayLectures, section, i);
        tableHtml += "</tr>";
    }

    table.find("tbody").html(tableHtml);
    postFunction(table, section);

}

function prepareDay(dayLectures, section, dayID) {
    let day = "";
    let lecturePerPlace = filterByPlace(dayLectures);
    let sectionPlaces = places.get(section).places;
    console.log(sectionPlaces);
    console.log(dayLectures);
    sectionPlaces.forEach(function (place, index) {
        let placeRow = "";
        // let lecturePlaces = places.get(value.id);
        // for (let l in lecturePlaces) {
        let d = lecturePerPlace.get(place.id);

        day += "<tr place-scope='" + place.id + "'"
            + " for-day='" + dayID + "' "
            + ">"
            + "<td>"
            + place.name
            + "</td>"
            + prepareRow(place, d, dayID, section)
            + "</tr>"
        // }

    });
    return day;
}

function prepareRow(place, lecturesInPlace, day, section) {
    let stringRow = "";
    // if ()

    if (typeof lecturesInPlace != 'undefined' && lecturesInPlace.length > 0) {

        for (let j2 = 0; j2 < 10;) {
            let data = filterByHour(lecturesInPlace, j2 + 8);
            let column = prepareColumn(data, j2 + 8, place.id, day, section);
            stringRow += column[0];
            j2 += column[1];
        }

    } else {
        for (let j2 = 0; j2 < 10; j2++) {
            let column = prepareColumn(null, j2 + 8, place.id, day, section);
            stringRow += column[0];
            // stringRow += "<td start='" + j2 + "' colspan='1'></td>";
        }
    }

    return stringRow;
}

function prepareColumn(lectures, start_time, place, day, section) {
    let maxTime = 1;
    let text = "";
    let target = -1;
    let group = -1;
    let classes = "align-middle p-0 ";
    let style = "--background: ";
    let bg_color = "transparent";
    let lecture_id = -1;
    let subject_id = -1;
    // let start_time = 1;
    if (lectures != null) {
        lectures.forEach(function (lecture, index) {
            if (index > 0)
                text += "<br>";

            text += lecture.name;
            lecture_id = lecture.id;
            // start_time = lecture._from;
            let lectureTime = lecture._to - lecture._from;
            maxTime = max(maxTime, lectureTime);

            target = lecture.subject_id;
            bg_color = lecture.background_color;
            if (lecture.group !== 'null') {
                group = lecture.group;
            }
        });

        if (lectures.length > 1)
            classes += "bg-danger";
    }

    return [
        "<td target='lecture-" + target + "' "
        + "group='" + group + "'"
        + "section='" + section + "'"
        + "day='" + day + "'"
        + "subject='" + target + "'"
        + "start='" + start_time + "'"
        + "place='" + place + "'"
        + "lecture-id='" + lecture_id + "'"
        + "dragable='True'"
        + " class='" + classes + "'"
        + " style='" + style + bg_color
        + "' scope='col' colspan='" + maxTime + "'>" + text + "</td>", maxTime
    ]
}

function filterByHour(lecturesInDay, hour) {
    let data = Array();
    lecturesInDay.forEach(function (value, index) {
        if (value._from === hour) {
            data.push(value);
        }
    });
    return data;
}

function max(x, y) {
    if (x > y)
        return x;
    else
        return y;
}


function filterByDay(_subjects, section) {
    if (typeof _subjects == 'undefined')
        return Array();
    let lecturesPerDays = Array();
    for (let i = 0; i < 6; i++) {
        let lecturesPerDay = Array();
        lecturesPerDays.push(lecturesPerDay)
    }
    let subjects = [..._subjects];
    subjects.forEach(function (value) {
        let bg = getRandomColor();
        value.lectures.forEach(function (lect) {
            let day = lect.day;
            let edited = checkEdited(lect.id);
            if (edited != null) {
                day = edited.day + 1;
            }

            let lecturesPerDay = lecturesPerDays[day - 1];
            if (!(typeof lecturesPerDay == 'undefined')) {
                if (!(lect in lecturesPerDay)) {
                    lect.day = day;
                    lect.name = value.name;
                    lect.code = value.code;
                    lect.subject_id = value.id;
                    lect.background_color = bg;
                    if (edited != null) {
                        lect.to = edited.from + edited.length;
                        lect.place = getPlace(section, edited.place);
                    }
                    lecturesPerDay.push(lect);
                }

            }

        });
        for (let i = 0; i < 6; i++) {
            lecturesPerDays[i] = lecturesPerDays[i].sort(compare);
        }


    });


    return lecturesPerDays

}

function getPlace(section, id) {
    for (let i in places.get(section).places) {
        let place = places.get(section).places[i];
        if (place.id === Number(id)) {
            return place

        }
    }
    return null;
}

function checkEdited(id) {
    for (let i in edited_lectures) {

        let v = edited_lectures[i];

        if (v.id == id)
            return v
    }
    return null
}

function filterByPlace(dayLectures) {
    let placesMap = new Map();

    for (let i in dayLectures) {
        let lect = dayLectures[i];
        if (placesMap.has(lect.place)) {
            placesMap.get(lect.place).push(lect);
        } else {
            let array = [lect];
            placesMap.set(lect.place, array);
        }


    }

    return placesMap;

}


function loadLectures(sectionId, response) {
    let url = host + "/api/subjects/" + sectionId;
    $.get(url)
        .done(function (data) {
            // if (!(sectionId in places)) {
            lectures.set(sectionId, data);
            // }
            response();
        });
}

function loadPlaces(sectionId, table, response) {
    let url = host + "/api/sections/" + sectionId;
    $.get(url)
        .done(function (data) {
            // if (!(sectionId in places)) {
            places.set(sectionId, data);
            // }
            response();
        });
}


function getRandomColor() {
    let red = Math.round(Math.random() * 255);
    let blue = Math.round(Math.random() * 255);
    let green = Math.round(Math.random() * 255);
    let alpha = 0.5;
    let step = 200;
    if (red > step && blue > step && green > step)
        return getRandomColor();
    return 'rgba(' + red + ',' + blue + ',' + green + ',' + alpha + ')';
}

function compare(a, b) {
    if (a._from < b._from)
        return 1;
    if (a._from > b._from)
        return -1;
    return 0;
}


$(document).ready(function () {
    $("table[tag]").each(function () {
        let table = $(this);
        let id = table.attr('data-target');
        // alert(tag);
        loadPlaces(id, table, function () {
            loadLectures(id, function () {
                let post = function () {
                    let selector = $("td");
                    selector.draggable({
                        revert: true,
                        cursor: "move",

                    });
                    selector.droppable({
                        drop: function (e, ui) {
                            let dropped = ui.draggable;
                            let droppedOn = $(this);
                            let src_id = dropped.attr("lecture-id");
                            let dist = {
                                id: dropped.attr("lecture-id"),
                                day: droppedOn.attr("day"),
                                from: Number(droppedOn.attr("start")),
                                length: Number(dropped.attr("colspan")),
                                subject: Number(dropped.attr("subject")),
                                section: dropped.attr("section"),
                                group: dropped.attr("group"),
                                place: droppedOn.attr("place"),
                            };

                            // edited_lectures.push(dist);
                            let table = $("table[data-target=" + dist.section + "]");

                            if (droppedOn.text().trim() === "") {

                                if (dist.length > 1) {
                                    dropped.attr("colspan", 1);
                                    for (let i = 0; i < dist.length - 1; i++) {
                                        let start = dropped.attr("start") + i;
                                        let classes = "align-middle p-0 ";
                                        let style = "--background: ";
                                        let bg_color = "red";
                                        let td = "<td target='lecture--1' "
                                            + "group='" + -1 + "'"
                                            + "section='" + dist.section + "'"
                                            + "day='" + dropped.attr("day") + "'"
                                            + "start='" + start + "'"
                                            + "place='" + dropped.attr("place") + "'"
                                            + "lecture-id='" + -1 + "'"
                                            // + "dragable='True'"
                                            + " class='" + classes + "'"
                                            + " style='" + style + bg_color
                                            + "' scope='col' colspan='" + 1 + "'></td>";
                                        dropped.after(td);


                                    }

                                }

                                droppedOn.text(dropped.text());
                                dropped.text("");

                                droppedOn.attr("lecture-id", dropped.attr("lecture-id"));
                                dropped.attr("lecture-id", -1);
                                droppedOn.attr("target", dropped.attr("target"));
                                dropped.attr("target", 'lecture--1');
                                droppedOn.attr("style", dropped.attr("style"));
                                droppedOn.css({
                                    top: 'auto',
                                    left: 'auto'
                                });
                                dropped.attr("style", "");


                                // droppedOn.attr("start");
                                droppedOn.attr("colspan", dist.length);
                                if (dist.length > 1) {
                                    for (let i = 1; i < dist.length; i++) {

                                        droppedOn.next().detach();
                                    }
                                }

                                dropped.attr("section");
                                droppedOn.attr("group", dropped.attr("group"));
                                droppedOn.attr("place");


                                let success = function (data) {
                                    console.log("Received data:");
                                    console.log(data);

                                };

                                let error = function (e) {
                                    console.log("Received error:");
                                    console.log(e);
                                };

                                update_lecture(src_id,
                                    dist.from,
                                    dist.from + dist.length,
                                    dist.section,
                                    dist.place,
                                    dist.day,
                                    dist.subject,
                                    dist.group,
                                    success,
                                    error);


                                $('*').unbind('**');
                                prepareEvents();
                            }


                        }
                    });
                };
                drawTable(table, id, post);
                prepareEvents();
                // selector.css({position: 'static'});
                // selector.attr("style", "");

            });
        });
    });
});


function prepareEvents() {
    let hiLightClass = "bg-warning";
    let selectClass = "selected-column";
    let columnSelector = $("#mainTable").find("td[target]");
    columnSelector.mouseenter(function (e) {

        let id = $(this).attr("target");

        let target = $("td[target='" + id + "']");
        if (!target.hasClass(selectClass))
            target.addClass(hiLightClass);

    });

    columnSelector.mouseleave(function (e) {
        let id = $(this).attr("target");
        $("td[target='" + id + "']").removeClass(hiLightClass);

    });

    columnSelector.click(function (e) {

        let id = $(this).attr("target");
        let group = $(this).attr("group");
        let target = $("td[target='" + id + "'][group='" + group + "']");

        if (target.hasClass(selectClass)) {
            target.removeClass(selectClass);
            target.addClass(hiLightClass);
        } else {
            target.addClass(selectClass);
            target.removeClass(hiLightClass);
        }


    });

    prepare_submenu_event();
    prepare_lecture_form_events();
    prepare_confirm_lecture_form_events();
}


function prepare_submenu_event() {
    let columnSelector = $("td[target]");
    columnSelector.contextmenu(function (e) {
        e.preventDefault();
        let selectedCell = $(this);

        selectedCell.addClass("focus");

        contextMenu(event.pageX, event.pageY, selectedCell);

    });

    $("*").click(function (e) {
        let menu = $("#context-menu");
        menu.removeClass("show");
        $(".focus").removeClass("focus");
    });

}


function contextMenu(x, y, node) {
    let menu = $("#context-menu");
    let editLecture = $("#editLecture");
    let addLecture = $("#addLecture");
    let deleteLecture = $("#deleteLecture");

    let form = $("#lecture-form");
    let form_selectedSubject = $("#selected_subject");
    let form_group_number = $("#group_number");
    let form_lecture_length = $("#lecture_length");
    let form_submit_button = $("#submit_form");
    let form_error_message = $("#lecture_error");

    let popup_window = $("#popup-window");
    let popup_confirm = $("#popup-confirm");

    // Setting place and  showing the popup menu
    menu.offset({top: y, left: x});
    menu.addClass("show");

    let lecture_id = node.attr("lecture-id");
    let subject = Number(node.attr("subject"));
    let section = node.attr("section");
    let day = node.attr("day");
    let place = node.attr("place");
    let length = Number(node.attr("colspan"));
    let start = Number(node.attr("start"));
    let end = start + length;
    let group = Number(node.attr("group"));


    // Remove old event handler;
    editLecture.unbind("click");
    addLecture.unbind("click");
    deleteLecture.unbind("click");
    form.unbind("submit");

    if (subject === -1) {
        editLecture.attr("disabled", true);
        deleteLecture.attr("disabled", true);
        addLecture.removeAttr("disabled");
    } else {
        addLecture.attr("disabled", true);
        editLecture.removeAttr("disabled");
        deleteLecture.removeAttr("disabled");
    }

    form_selectedSubject.html("");
    let subjects = lectures.get(section);
    subjects.forEach(function (val) {
        // form_selectedSubject.append('<option value="' + val.id + '" selected="selected">Foo</option>');
        form_selectedSubject.append('<option value="' + val.id + '" >' + val.name + '</option>');
    });


    // Create new event handler for editing button
    editLecture.click(function (e) {

        form_selectedSubject.val(subject);
        form_selectedSubject.attr("disabled", true);
        form_error_message.hide();
        popup_window.addClass("show");

        form_lecture_length.val(length);

        if (Number.isNaN(group)) {
            form_group_number.val(1);
        } else {
            if (group < 1)
                group = 1;
            form_group_number.val(group);
        }

        form_lecture_length.unbind("change");
        form_lecture_length.change(function () {

            let value = Number($(this).val());
            let nextHour = node.next();
            if (value > length) {
                // Check if no lecture after
                if (nextHour.attr("lecture-id") !== '-1') {
                    form_error_message.show();
                    form_submit_button.attr("disabled", true);
                }
            } else {
                form_error_message.hide();
                form_submit_button.removeAttr("disabled");

            }


        });


        form.submit(function (e) {
            e.preventDefault();


            let success = function (data) {
                console.log("Received data:");
                console.log(data);

                node.attr("group", data.group);
                node.attr("colspan", data._to - data._from);
                let place = node.attr("place");
                let day = node.attr("day");
                let section = node.attr("section");

                if (data._to - data._from < length) {


                    for (let i = 0; i < length - (data._to - data._from); i++) {
                        let cell = prepareColumn(null, data._to + i, place, day, section);

                        node.after(cell[0]);
                    }


                } else if (data._to - data._from > length) {

                    for (let i = 0; i < (data._to - data._from) - length; i++) {
                        let nextNode = node.next();
                        console.log(nextNode);
                        nextNode.detach();
                    }

                }


                popup_window.removeClass("show");
            };

            let error = function (e) {

            };
            group = form_group_number.val();
            end = start + Number(form_lecture_length.val());
            subject = form_selectedSubject.val();


            update_lecture(lecture_id,
                start,
                end,
                section,
                place,
                day,
                subject,
                group,
                success,
                error);

        });
    });

    // Create new event handler for adding button
    addLecture.click(function (e) {
        form_selectedSubject.removeAttr("disabled");
        form_lecture_length.val(1);
        form_group_number.val(1);
        popup_window.addClass("show");

        // Add event when submit form
        form.submit(function (e) {
            e.preventDefault();

            let place = node.attr("place");
            let day = Number(node.attr("day")) + 1;
            let section = node.attr("section");
            let success = function (data) {
                console.log("Received data:");
                console.log(data);

                node.attr("group", data.group);
                node.attr("colspan", data._to - data._from);
                node.attr("target", "lecture-" + data.subject);
                node.attr("lecture-id", data.id);
                node.attr("subject", data.subject);


                let text = $('#selected_subject option:selected').text();
                node.text(text);

                if (data._to - data._from < length) {


                    for (let i = 0; i < length - (data._to - data._from); i++) {
                        let cell = prepareColumn(null, data._to + i, place, day, section);
                        console.log(cell);
                        node.after(cell[0]);
                    }

                } else if (data._to - data._from > length) {

                    for (let i = 0; i < (data._to - data._from) - length; i++) {
                        let nextNode = node.next();
                        console.log(nextNode);
                        nextNode.detach();
                    }
                }


                popup_window.removeClass("show");
            };

            let error = function (e) {

            };
            group = form_group_number.val();
            end = start + Number(form_lecture_length.val());
            subject = form_selectedSubject.val();


            create_lecture(lecture_id,
                start,
                end,
                section,
                place,
                day,
                subject,
                group,
                success,
                error);

        });
    });


    deleteLecture.click(function (e) {

        let form_lecture_time_from = $('#lecture_time_from');
        let form_lecture_time_to = $('#lecture_time_to');
        let form_lecture_name = $('#lecture_name');

        form_lecture_name.text(node.text());

        form_lecture_time_from.text(start);
        form_lecture_time_to.text(end);


        $("#confirm_delete").click(function (e) {
            e.preventDefault();

            let id = Number(node.attr("lecture-id"));
            let success = function (data) {

                // node.attr("lecture-id", -1);
                let start = Number(node.attr("from"));

                for (let i = 0; i < length ; i++) {
                    let cell = prepareColumn(null, start + i, place, day, section);

                    node.after(cell[0]);
                }

                node.detach();
                popup_confirm.removeClass("show");
            };

            let error = function (e) {

            };
            delete_lecture(id, success, error);

        });


        popup_confirm.addClass("show");
    });

}


function prepare_lecture_form_events() {
    let popup_window = $("#popup-window");
    $("#cancelEdit").click(function (e) {
        e.preventDefault();
        popup_window.removeClass("show");
    });
}


function prepare_confirm_lecture_form_events() {
    let popup_confirm = $("#popup-confirm");
    $("#cancel_delete").click(function (e) {
        e.preventDefault();
        popup_confirm.removeClass("show");
    });
}


function update_lecture(id, from, to, section, place, day, subject = -1, group = -1, success_function = null, error_function = null) {
    let loading_view = $("#loading-view");

    if (Number.isNaN(Number(group)))
        group = -1;
    else
        group = Number(group);

    $.ajax({
            url: host + "/api/lectures/" + id + "/edit",
            method: "PUT",
            data: {
                subject_id: id,
                group: group,
                place: place,
                subject: subject,
                day: day,
                _from: from,
                _to: to,
            }
        }
    ).done(function (data) {
        loading_view.removeClass("show");
        if (success_function != null)
            success_function(data);

    }).fail(function (e) {
        loading_view.removeClass("show");
        if (error_function != null)
            error_function(e);

    });

}


function create_lecture(id, from, to, section, place, day, subject = -1, group = -1, success_function = null, error_function = null) {
    let loading_view = $("#loading-view");

    if (Number.isNaN(Number(group)))
        group = -1;
    else
        group = Number(group);

    $.ajax({
            url: host + "/api/lectures/create",
            method: "POST",
            data: {
                group: group,
                place: place,
                subject: subject,
                day: day,
                _from: from,
                _to: to,
            }
        }
    ).done(function (data) {
        loading_view.removeClass("show");
        if (success_function != null)
            success_function(data);

    }).fail(function (e) {
        loading_view.removeClass("show");
        if (error_function != null)
            error_function(e);

    });

}


function delete_lecture(id, success_function = null, error_function = null) {
    let loading_view = $("#loading-view");

    $.ajax({
            url: host + "/api/lectures/" + id + "/delete",
            method: "DELETE"
        }
    ).done(function (data) {
        loading_view.removeClass("show");
        if (success_function != null)
            success_function(data);

    }).fail(function (e) {
        loading_view.removeClass("show");
        if (error_function != null)
            error_function(e);

    });

}
