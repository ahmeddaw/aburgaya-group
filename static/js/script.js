// $(".dropdown-toggle").hover($(this).click());
// $(".dropdown-toggle").mouseenter(function () {
//     $(this).click()
// }).mouseleave(function () {
//     $(this).click()
// });

$(document).ready(function () {
    $('.nav li.dropdown').hover(function () {
        $(this).addClass('open');
    }, function () {
        $(this).removeClass('open');
    });

    let sort = $("#sort-by");
    let sort_name = sort.attr("name");
    let param = new URLSearchParams(location.search);
    let sort_by = param.get(sort_name);
    if ( sort_by !== null){
        sort.val(sort_by)
    }

    let section = $("#section");
    let section_name = section.attr("name");
    let section_param = new URLSearchParams(location.search);
    let section_is = section_param.get(section_name);
    if ( section_is !== null){
        section.val(section_is)
    }

});

function selectSort(obj) {

    let val = obj.value;
    let name = obj.name;
    let param = new URLSearchParams(location.search);
    param.set(name, val);
    location.href = location.origin + location.pathname + "?" + param.toString()

}


function grid(id, col) {
    let container = $("#" + id);
    container.removeClass('col2');
    container.removeClass('col3');
    container.removeClass('col4');
    container.removeClass('col5');

    container.addClass(col)
}


// jQuery.fn.center = function() {
//     return this.each(function(){
//             var el = $(this);
//             var h = el.height();
//             var w = el.width();
//             var w_box = $(window).width();
//             var h_box = $(window).height();
//             var w_total = (w_box - w)/2; //400
//             var h_total = (h_box - h)/2;
//             var css = {"position": 'absolute', "left": w_total+"px"};
//             el.css(css)
//     });
// };






