from django.db import models


class Summary(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=2048)
    writer = models.CharField(max_length=2048, default="", blank=True)
    body = models.TextField(default="", blank=True)
    image = models.FileField(upload_to="summaries/banners")
    icon = models.FileField(upload_to="summaries/icons")
    views = models.IntegerField(default=0)

    def __str__(self):
        return self.title






