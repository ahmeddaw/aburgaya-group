from django.urls import path
from .views import *

app_name= 'Summaries'
urlpatterns = [
    path('', SummariesListView.as_view(), name='home'),
    path('<int:pk>/detail', SummariesDetailView.as_view(), name='detail'),
    path('add', SummariesAddView.as_view(), name='add'),
    path('<int:pk>/edit', SummariesEditView.as_view(), name='edit'),
    path('<int:pk>/delete', SummariesDeleteView.as_view(), name='delete'),
]
