from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView, DetailView, ListView

from .forms import *
from Products.models import Product
from Summaries.models import Summary


class SummariesListView(ListView):
    template_name = "summaries_list.html"
    model = Summary
    paginate_by = 10
    context_object_name = "summaries"

    def get_context_data(self, *, object_list=None, **kwargs):
        data = super().get_context_data(object_list=object_list, **kwargs)
        if 'q' in self.request.GET.keys():
            q = self.request.GET['q']
            data['q'] = q
        return data

    def get_queryset(self):
        if 'q' in self.request.GET.keys():
            q = self.request.GET['q']
            bks = Summary.objects.filter(
                Q(writer__icontains=q) |
                Q(title__icontains=q) |
                Q(body__icontains=q)
            )
            return bks
        return super().get_queryset()


class SummariesDetailView(DetailView):
    template_name = "authors_detail.html"
    model = Summary
    context_object_name = 'summary'


class SummariesAddView(CreateView):
    template_name = "admin/summaries_add_form.html"
    model = Summary
    form_class = AddSummaryForm

    def get_success_url(self):
        return reverse("Manager:manager_summaries")


class SummariesEditView(UpdateView):
    template_name = "admin/summaries_add_form.html"
    model = Summary
    form_class = AddSummaryForm

    def get_success_url(self):
        return reverse("Manager:manager_summaries")


class SummariesDeleteView(DeleteView):
    model = Summary
    success_url = reverse_lazy('Manager:manager_summaries')
    template_name = 'admin/delete_form.html'
    extra_context = {
        'back_url': reverse_lazy('Manager:manager_summaries')
    }