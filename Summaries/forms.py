from django import forms
from Summaries.models import Summary


class AddSummaryForm(forms.ModelForm):
    title = forms.CharField(max_length=2048)
    body = forms.CharField(widget=forms.Textarea())
    image = forms.FileField(required=False)
    icon = forms.FileField(required=False)

    title.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Name',
    }

    body.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Description',
        'rows': 20
    }

    image.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Image',
    }

    icon.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Icon',
    }

    class Meta:
        model = Summary
        # fields = '__all__'
        exclude = ['views']

    def clean(self):
        s = super().clean()

        print(self.errors)
        return s

    def save(self, commit=True):
        return super().save(commit)
