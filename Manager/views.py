from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.views.generic import TemplateView, ListView

# from Authors.models import Author
from Company.models import Company
from Products.models import Product
from Categories.models import Category

from Summaries.models import Summary


class HomeView(TemplateView):
    template_name = "admin/home.html"


class ProductsListView(ListView):
    template_name = "admin/books_admin_list.html"
    model = Product
    context_object_name = 'books'
    paginate_by = 10


class CategoriesListView(ListView):
    template_name = "admin/categories_admin_list.html"
    model = Category
    context_object_name = 'categories'
    paginate_by = 10


class UsersListView(ListView):
    template_name = "admin/users_admin_list.html"
    model = get_user_model()
    context_object_name = 'users'
    paginate_by = 10
    # User.is_authenticated


class CompanyListView(ListView):
    template_name = "admin/authors_admin_list.html"
    model = Company
    context_object_name = 'authors'
    paginate_by = 10

class SummariesListView(ListView):
    template_name = "admin/summaries_admin_list.html"
    model = Summary
    context_object_name = 'summaries'
    paginate_by = 10
