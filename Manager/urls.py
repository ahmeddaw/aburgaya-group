from django.contrib.auth.decorators import login_required
from django.urls import path

from Manager.views import *

app_name = 'Manager'
urlpatterns = [
    path('', login_required(HomeView.as_view()), name='manager_home'),
    path('products', login_required(ProductsListView.as_view()), name='manager_products'),
    path('categories', login_required(CategoriesListView.as_view()), name='manager_categories'),
    path('users', login_required(UsersListView.as_view()), name='manager_users'),
    path('company', login_required(CompanyListView.as_view()), name='manager_company'),
    # path('publishers', login_required(PublisherListView.as_view()), name='manager_publishers'),
    path('summaries', login_required(SummariesListView.as_view()), name='manager_summaries'),
]
