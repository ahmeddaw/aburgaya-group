from django.db import models
from django.urls import reverse


class Product(models.Model):
    id = models.BigAutoField(primary_key=True)
    isbn = models.CharField(max_length=50, blank=True)
    name = models.CharField(max_length=2048)
    category = models.ManyToManyField('Categories.Category', symmetrical=False)
    company = models.ManyToManyField('Company.Company', symmetrical=False)
    creator = models.ForeignKey('Accounts.User', on_delete=models.SET_NULL, null=True)
    description = models.TextField(blank=True)
    image = models.FileField(default="images/logo_only.png", upload_to="images")


    def __str__(self):
        return self.name

    @property
    def url(self):
        return reverse("media", kwargs={'path': self.image.url})
