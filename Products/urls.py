from django.contrib.auth.decorators import login_required
from django.urls import path

from Products.views import *

app_name = "Products"
urlpatterns = [
    path('', ProductsListView.as_view(), name='home'),
    path('<int:pk>/detail', ProductDetailView.as_view(), name='detail'),
    path('add', login_required(AddProductView.as_view()), name='add'),
    path('<int:pk>/edit', login_required(ProductEditView.as_view()), name='edit'),
    path('<int:pk>/delete', login_required(ProductDeleteView.as_view()), name='delete'),
    # path('list', ProductsListView.as_view(), name='list'),
]
