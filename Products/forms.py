from django import forms
from django.forms.utils import ErrorList

from Company.models import Company
from Products.models import Product
from Categories.models import Category
# from Publishers.models import Publisher


class AddProductForm(forms.ModelForm):
    isbn = forms.CharField(max_length=50)
    name = forms.CharField(max_length=2048)
    company = forms.ModelMultipleChoiceField(queryset=Company.objects.all(), )
    category = forms.ModelMultipleChoiceField(queryset=Category.objects.all())
    # company = forms.ModelChoiceField(queryset=Company.objects.all())
    description = forms.CharField(widget=forms.Textarea(), required=False)
    image = forms.FileField()

    isbn.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'ISBN',
    }

    name.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Name',
    }

    company.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Author',
    }

    category.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Category',
    }

    description.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Description',
    }

    image.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Image',
    }

    class Meta:
        model = Product
        # fields = '__all__'
        exclude = ['creator']

    def save(self, commit=True):
        if self.instance.id:
            b = self.instance
        else:
            b = Product()
        b.isbn = self.cleaned_data['isbn']
        b.name = self.cleaned_data['name']
        b.description = self.cleaned_data['description']
        # b.publisher = self.cleaned_data['publisher']
        b.image = self.cleaned_data['image']
        b.save()

        b.category.clear()
        b.category.add(*self.cleaned_data['category'])

        b.company.clear()
        b.company.add(*self.cleaned_data['company'])

        b.save()
        return b
