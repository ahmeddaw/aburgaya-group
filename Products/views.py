from django.db.models import Q
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, CreateView, ListView, UpdateView, DeleteView, DetailView
from Products.forms import AddProductForm
from Products.models import Product


class ProductDetailView(DetailView):
    template_name = "product_detail.html"
    model = Product

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        obj = self.get_object(self.get_queryset())
        data["similar_products"] = Product.objects.filter(Q(category__in=obj.category.all()))
        return data


class AddProductView(CreateView):
    model = Product
    template_name = 'admin/products_add_form.html'
    form_class = AddProductForm

    def get_success_url(self):
        return reverse("Manager:manager_products")


class ProductsListView(ListView):
    model = Product
    template_name = 'product_list.html'
    context_object_name = "products"
    paginate_by = 10

    def get_context_data(self, *, object_list=None, **kwargs):
        data = super().get_context_data(object_list=object_list, **kwargs)
        if 'q' in self.request.GET.keys():
            q = self.request.GET['q']
            data['q'] = q
        return data

    def get_queryset(self):
        if 'q' in self.request.GET.keys():
            q = self.request.GET['q']
            bks = Product.objects.filter(
                Q(isbn__icontains=q) |
                Q(name__icontains=q) |
                Q(description__icontains=q)
                # Q(author__description__icontains=q)
            )
            return bks
        return super().get_queryset()


class ProductEditView(UpdateView):
    model = Product
    template_name = 'admin/Products_add_form.html'
    form_class = AddProductForm

    def get_success_url(self):
        return reverse("Manager:manager_products")


class ProductDeleteView(DeleteView):
    model = Product
    success_url = reverse_lazy('Manager:manager_Products')
    template_name = 'admin/delete_form.html'
    extra_context = {
        'back_url': reverse_lazy('Manager:manager_Products')
    }

