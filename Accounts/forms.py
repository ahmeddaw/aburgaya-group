import time

from django import forms
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.hashers import make_password
from Accounts.models import User
from django.db import models


class AddUserForm(forms.ModelForm):
    email = forms.CharField(max_length=200)
    username = forms.CharField(max_length=2048)
    first_name = forms.CharField(max_length=2048)
    last_name = forms.CharField(max_length=2048)
    password = forms.CharField(max_length=1024, widget=forms.PasswordInput, required=False)
    password2 = forms.CharField(max_length=1024, widget=forms.PasswordInput, required=False)
    date_joined = forms.DateTimeField(required=False)

    email.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Email',
    }

    username.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'User Name',
    }

    first_name.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'First name',
    }
    last_name.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Last name',
    }
    password.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Password',
    }
    password2.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Retype Password',
    }

    class Meta:
        model = get_user_model()
        # fields = '__all__'
        exclude = ["category", "user_type"]

    def clean(self):
        data = super().clean()
        if 'password' in self.data.keys():
            pwd = self.data['password']

            data['is_staff'] = True
            data['is_active'] = True
            data['date_joined'] = time.time()
            data['password'] = make_password(pwd)

        else:
            if self.instance.id:
                data['password'] = self.instance.password
                data['password2'] = self.instance.password
        return super().clean()

    def clean_password(self):
        data = self.cleaned_data
        if not self.instance.id:
            pwd = self.data['password']
            pwd1 = self.data['password2']

            if not (pwd == pwd1 and len(pwd) >= 6):
                raise forms.ValidationError("Please enter valid password.")

        return data

    def save(self, commit=True):
        # self.cleaned_data['date_joined'] = time.time()
        return super().save(commit)


class LoginForm(forms.Form):
    username = forms.CharField(max_length=2048, required=True)
    password = forms.CharField(max_length=1024, widget=forms.PasswordInput, required=True)

    username.widget.attrs.update({
        'class': "form-control w-100 border",
        'placeholder': 'User Name',
    })


    password.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Password',
    }


    def is_valid(self):
        data = self.data
        # self.data
        username = data['username']
        pwd = data['password']
        try:
            # get_user_model()
            user = User.objects.get(email__iexact=username)
            return user.check_password(pwd)
        except User.DoesNotExist:
            return False

    def clean_user(self):
        try:
            username = self.data['username'].strip()
            # user = User.objects.get(username__iexact=username)
            password = self.data['password']
            u = authenticate(username=username, password=password)
            return u
            # if not user.check_password(password):
            #     return None
            # return user

        except User.DoesNotExist:
            raise forms.ValidationError('Email or Password is wrong')
