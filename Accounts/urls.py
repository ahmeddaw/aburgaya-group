from django.contrib.auth.decorators import login_required
from django.urls import path, re_path
from .views import *

app_name = 'Accounts'
urlpatterns = [
    re_path('^login', LoginView.as_view(), name='login'),
    path('logout', Logout.as_view(), name='logout'),
    path('add', login_required(AddUserView.as_view()), name='add'),
    path('<int:pk>/edit', login_required(EditUserView.as_view()), name='edit'),
    path('<int:pk>/delete', login_required(UserDeleteView.as_view()), name='delete'),
]
