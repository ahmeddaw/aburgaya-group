from django.contrib.auth import get_user_model, login, logout
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView, FormView, DeleteView
from django.views.generic.base import View, RedirectView

from Accounts.forms import AddUserForm, LoginForm


class AddUserView(CreateView):
    model = get_user_model()
    template_name = 'admin/users_add_form.html'
    form_class = AddUserForm

    def form_invalid(self, form):
        print(f"Form is invalid {form.errors}")
        return super().form_invalid(form)

    def get_success_url(self):
        return reverse("Manager:manager_users")


class EditUserView(UpdateView):
    model = get_user_model()
    template_name = 'admin/users_add_form.html'
    form_class = AddUserForm

    def form_invalid(self, form):
        print(f"Form is invalid {form.errors}")
        return super().form_invalid(form)

    def get_success_url(self):
        return reverse("Manager:manager_users")


class UserDeleteView(DeleteView):
    model = get_user_model()
    success_url = reverse_lazy('Manager:manager_users')
    template_name = 'admin/delete_form.html'
    extra_context = {
        'back_url': reverse_lazy('Manager:manager_users')
    }


class LoginView(FormView):
    template_name = 'admin/login.html'
    form_class = LoginForm

    def form_valid(self, form):
        user = form.clean_user()
        if user is None:
            return super().form_invalid(form)
        login(self.request, user)

        return super().form_valid(form)

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        if next_url:
            return next_url
        return reverse("Manager:manager_home")


class Logout(RedirectView):
    def get(self, request, *args, **kwargs):
        logout(request)
        return super().get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return reverse("Home:home")
