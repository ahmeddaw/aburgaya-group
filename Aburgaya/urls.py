"""BooksManage URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os

from django.contrib import admin
from django.urls import path, include, re_path
from django.views.static import serve

from Aburgaya import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("Home.urls")),
    path('products/', include("Products.urls")),
    # path('authors/', include("Company.urls")),
    # path('publisher/', include("Publishers.urls")),
    path('category/', include("Categories.urls")),
    path('accounts/', include("Accounts.urls")),
    path('company/', include("Company.urls")),
    path('manage/', include("Manager.urls")),


    re_path(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}, name='static'),
    re_path(r'^media/(?P<path>.*)$', serve, {'document_root': os.path.join(settings.MEDIA_ROOT)}, name="media"),
]
