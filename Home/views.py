from django.urls import reverse
from django.utils.translation import activate
from django.views.generic import TemplateView, ListView, RedirectView, FormView
from .forms import ContactForm
from Company.models import Company
from Products.models import Product
from Categories.models import Category
from django.core.mail import send_mail

class HomeView(TemplateView):
    template_name = "home.html"
    extra_context = {
        'products': Product.objects.all()[:4],
        # 'companies': Company.objects.all()[:8],
    }


class LibraryView(ListView):
    template_name = "library.html"
    model = Product
    paginate_by = 20
    context_object_name = "products"
    extra_context = {
        'products': Product.objects.all()[:8],
        'companies': Company.objects.all()[:8],
    }


class LanguageView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        if 'lang' in kwargs.keys():
            lang = kwargs['lang']
            activate(lang)

        data = self.request.GET
        if 'next' in data.keys():
            return data['next']

        return reverse("Home:home")



class AboutView(FormView):
    template_name = "about.html"
    form_class = ContactForm
    success_url = "/"

    def form_valid(self, form):
        data = form.cleaned_data
        email = data["email"]
        name = data["name"]
        description = data["description"]
        category = data["category"]
        send_mail(
    '${name}',
    '${description}',
    '${email}',
    ['support@aburgayagroup.com'],
    fail_silently=False,
)

        # if user is None:
        #     return super().form_invalid(form)
        # login(self.request, user)

        return super().form_valid(form)
    