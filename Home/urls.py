from django.urls import path

from Home.views import *


app_name = "Home"
urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('library', LibraryView.as_view(), name='library'),
    path('setlang=<str:lang>', LanguageView.as_view(), name="setLanguage"),
    path('about', AboutView.as_view(), name="about"),
]
