from Categories.models import Category
from django import forms

class ContactForm(forms.Form):
    email = forms.CharField(max_length=200)
    name = forms.CharField(max_length=2048)
    description = forms.CharField(max_length=2048*10, widget=forms.Textarea())
    category = forms.ModelMultipleChoiceField(queryset=Category.objects.all())
    

    email.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Email',
    }

    name.widget.attrs = {
        'class': 'form-control',
        'placeholder': 'Name',
    }

    description.widget.attrs = {
        'class': 'form-control',
        'placeholder': '...',
        'rows':5
    }

    category.widget.attrs = {
        'class': 'form-control',
        'placeholder': '...',
    }
    
    def clean(self):
        data = super().clean()
        if 'password' in self.data.keys():
            pwd = self.data['password']

            data['is_staff'] = True
            data['is_active'] = True
            # data['date_joined'] = time.time()
            # data['password'] = make_password(pwd)

        # else:
        #     if self.instance.id:
        #         data['password'] = self.instance.password
        #         data['password2'] = self.instance.password
        return super().clean()

    def clean_password(self):
        data = self.cleaned_data
        if not self.instance.id:
            pwd = self.data['password']
            pwd1 = self.data['password2']

            if not (pwd == pwd1 and len(pwd) >= 6):
                raise forms.ValidationError("Please enter valid password.")

        return data

    def save(self, commit=True):
        # self.cleaned_data['date_joined'] = time.time()
        return super().save(commit)
